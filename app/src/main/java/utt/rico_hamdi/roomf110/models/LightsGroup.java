package utt.rico_hamdi.roomf110.models;

public class LightsGroup extends HueObject {
    public LightsGroup(String id, String name) {
        super(id, name);
    }

    public static LightsGroup getGroupAll(){
        return new LightsGroup("0","All");
    }
}
