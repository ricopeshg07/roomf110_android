package utt.rico_hamdi.roomf110.models;

public class HueObject {
    public String id;
    public String name;

    public HueObject(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
