package utt.rico_hamdi.roomf110.aws;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import utt.rico_hamdi.roomf110.Constant;
import utt.rico_hamdi.roomf110.MainActivity;
import utt.rico_hamdi.roomf110.models.Light;
import utt.rico_hamdi.roomf110.models.LightsGroup;
import utt.rico_hamdi.roomf110.models.Scene;

public class AwsService {
    static final String LOG_TAG = AwsService.class.getCanonicalName();

    MainActivity mActivity;
    Context mContext;

    public AwsService (Context appContext) {
        mActivity = (MainActivity) appContext;
        mContext = appContext;
    }


    public static void setMqttManager(AWSIotMqttManager mqttManager) {
        AwsService.mqttManager = mqttManager;
    }

    private static AWSIotMqttManager mqttManager;


    public static void getAllLights(AWSIotMqttNewMessageCallback callback) {

        subscribe(Constant.TOPIC_BRIDGE_SHADOW_GET_ACCEPTED, callback);
        publish(Constant.TOPIC_BRIDGE_SHADOW_GET, "");

        return;
    }

    public static void getAllGroups(AWSIotMqttNewMessageCallback callback) {
        subscribe(Constant.TOPIC_BRIDGE_FEATURES_GROUPES_GET_ACCEPTED, callback);
        publish(Constant.TOPIC_BRIDGE_FEATURES_GROUPES_GET, "");
        return;
    }

    public static void getAllScenes(AWSIotMqttNewMessageCallback callback) {
        subscribe(Constant.TOPIC_BRIDGE_FEATURES_SCENES_GET_ACCEPTED, callback);
        publish(Constant.TOPIC_BRIDGE_FEATURES_SCENES_GET, "");
        return;
    }

    public static void addScene(String json) {

        publish(Constant.TOPIC_BRIDGE_FEATURES_SCENES_NEW, json);

        return;
    }

    public static void addGroup(String json) {

        publish(Constant.TOPIC_BRIDGE_FEATURES_GROUPES_NEW, json);
        return;
    }


    public static void deleteScene(String id){

        publish(Constant.TOPIC_BRIDGE_FEATURES_SCENES_DELETE, id);

        return;

    }
    public static void deleteGroup(String id) {
        publish(Constant.TOPIC_BRIDGE_FEATURES_GROUPES_DELETE, id);
        return;
    }

    public static void setLightState(HashMap<String,Object> values) {

        publish(Constant.TOPIC_BRIDGE_SHADOW_UPDATE, myToJson(values));
        return;
    }

    public static void setGroupState(HashMap<String,Object> values) {

        publish(Constant.TOPIC_BRIDGE_FEATURES_GROUPES_SET, myToJson(values));

    }

    public static void recallScene(String id){

        publish(Constant.TOPIC_BRIDGE_FEATURES_SCENES_SET, id);
        return;
    }

    public static void subscribeTemperature(AWSIotMqttNewMessageCallback callback){

        subscribe(Constant.TOPIC_GROVEPI_SHADOW_GET_ACCEPTED, callback);
        publish(Constant.TOPIC_GROVEPI_SHADOW_GET, "");

        return;
    }

    private static void subscribe(String topic, AWSIotMqttNewMessageCallback callback){

        try {
            mqttManager.subscribeToTopic(topic, AWSIotMqttQos.QOS0, callback);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Subscription error.", e);
        }

    }


    private static void publish(String topic, String data){
        try {
            mqttManager.publishString(data, topic, AWSIotMqttQos.QOS0);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Subscription error.", e);
        }
    }

    private static String myToJson(HashMap<String,Object> o){
        Gson json = new Gson();
        return json.toJson(o);
    }
}
