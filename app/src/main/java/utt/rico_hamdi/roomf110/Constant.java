package utt.rico_hamdi.roomf110;

public class Constant {

    //Bridge Shadow Topic
    private static final String TOPIC_BRIDGE_SHADOW = "$aws/things/PhilipsHueBridge/shadow";

    //Bridge Shadow update Topics
    public static final String TOPIC_BRIDGE_SHADOW_UPDATE = TOPIC_BRIDGE_SHADOW + "/update";
    public static final String TOPIC_BRIDGE_SHADOW_UPDATE_ACCEPTED = TOPIC_BRIDGE_SHADOW_UPDATE + "/accepted";
    public static final String TOPIC_BRIDGE_SHADOW_UPDATE_REJECTED = TOPIC_BRIDGE_SHADOW_UPDATE + "/rejected";

    //Bridge Shadow get Topics
    public static final String TOPIC_BRIDGE_SHADOW_GET = TOPIC_BRIDGE_SHADOW + "/get";
    public static final String TOPIC_BRIDGE_SHADOW_GET_ACCEPTED = TOPIC_BRIDGE_SHADOW_GET + "/accepted";
    public static final String TOPIC_BRIDGE_SHADOW_GET_REJECTED = TOPIC_BRIDGE_SHADOW_GET + "/rejected";

    //Bridge Shadow delete Topics
    public static final String TOPIC_BRIDGE_SHADOW_DELETE = TOPIC_BRIDGE_SHADOW + "/delete";
    public static final String TOPIC_BRIDGE_SHADOW_DELETE_ACCEPTED = TOPIC_BRIDGE_SHADOW_DELETE + "/accepted";
    public static final String TOPIC_BRIDGE_SHADOW_DELETE_REJECTED = TOPIC_BRIDGE_SHADOW_DELETE + "/rejected";



    //GrovePiSensors Shadow Topic
    private static final String TOPIC_GROVEPI_SHADOW = "$aws/things/GrovePiSensors/shadow";

    //Bridge Shadow update Topics
    public static final String TOPIC_GROVEPI_SHADOW_UPDATE = TOPIC_GROVEPI_SHADOW + "/update";
    public static final String TOPIC_GROVEPI_SHADOW_UPDATE_ACCEPTED = TOPIC_GROVEPI_SHADOW_UPDATE + "/accepted";
    public static final String TOPIC_GROVEPI_SHADOW_UPDATE_REJECTED = TOPIC_GROVEPI_SHADOW_UPDATE + "/rejected";

    //Bridge Shadow get Topics
    public static final String TOPIC_GROVEPI_SHADOW_GET = TOPIC_GROVEPI_SHADOW + "/get";
    public static final String TOPIC_GROVEPI_SHADOW_GET_ACCEPTED = TOPIC_GROVEPI_SHADOW_GET + "/accepted";
    public static final String TOPIC_GROVEPI_SHADOW_GET_REJECTED = TOPIC_GROVEPI_SHADOW_GET + "/rejected";

    //Bridge Shadow delete Topics
    public static final String TOPIC_GROVEPI_SHADOW_DELETE = TOPIC_GROVEPI_SHADOW + "/delete";
    public static final String TOPIC_GROVEPI_SHADOW_DELETE_ACCEPTED = TOPIC_GROVEPI_SHADOW_DELETE + "/accepted";
    public static final String TOPIC_GROVEPI_SHADOW_DELETE_REJECTED = TOPIC_GROVEPI_SHADOW_DELETE + "/rejected";


    private static final String TOPIC_BRIDGE_FEATURES = "things/PhilipsHueBridge/features";

    public static final String TOPIC_BRIDGE_FEATURES_GROUPES = TOPIC_BRIDGE_FEATURES + "/groups";

    public static final String TOPIC_BRIDGE_FEATURES_GROUPES_GET = TOPIC_BRIDGE_FEATURES_GROUPES + "/get";
    public static final String TOPIC_BRIDGE_FEATURES_GROUPES_GET_ACCEPTED = TOPIC_BRIDGE_FEATURES_GROUPES_GET + "/accepted";
    public static final String TOPIC_BRIDGE_FEATURES_GROUPES_GET_REJECTED = TOPIC_BRIDGE_FEATURES_GROUPES_GET + "/rejected";

    public static final String TOPIC_BRIDGE_FEATURES_GROUPES_DELETE = TOPIC_BRIDGE_FEATURES_GROUPES + "/delete";
    public static final String TOPIC_BRIDGE_FEATURES_GROUPES_DELETE_ACCEPTED = TOPIC_BRIDGE_FEATURES_GROUPES_DELETE + "/accepted";
    public static final String TOPIC_BRIDGE_FEATURES_GROUPES_DELETE_REJECTED = TOPIC_BRIDGE_FEATURES_GROUPES_DELETE + "/rejected";

    public static final String TOPIC_BRIDGE_FEATURES_GROUPES_NEW = TOPIC_BRIDGE_FEATURES_GROUPES + "/new";

    public static final String TOPIC_BRIDGE_FEATURES_GROUPES_SET = TOPIC_BRIDGE_FEATURES_GROUPES + "/set";

    public static final String TOPIC_BRIDGE_FEATURES_SCENES = TOPIC_BRIDGE_FEATURES + "/scenes";

    public static final String TOPIC_BRIDGE_FEATURES_SCENES_GET = TOPIC_BRIDGE_FEATURES_SCENES + "/get";
    public static final String TOPIC_BRIDGE_FEATURES_SCENES_GET_ACCEPTED = TOPIC_BRIDGE_FEATURES_SCENES_GET + "/accepted";
    public static final String TOPIC_BRIDGE_FEATURES_SCENES_GET_REJECTED = TOPIC_BRIDGE_FEATURES_SCENES_GET + "/rejected";

    public static final String TOPIC_BRIDGE_FEATURES_SCENES_DELETE = TOPIC_BRIDGE_FEATURES_SCENES + "/delete";
    public static final String TOPIC_BRIDGE_FEATURES_SCENES_DELETE_ACCEPTED = TOPIC_BRIDGE_FEATURES_SCENES_DELETE + "/accepted";
    public static final String TOPIC_BRIDGE_FEATURES_SCENES_DELETE_REJECTED = TOPIC_BRIDGE_FEATURES_SCENES_DELETE + "/rejected";

    public static final String TOPIC_BRIDGE_FEATURES_SCENES_NEW = TOPIC_BRIDGE_FEATURES_SCENES + "/new";

    public static final String TOPIC_BRIDGE_FEATURES_SCENES_SET = TOPIC_BRIDGE_FEATURES_SCENES + "/set";


}
