package utt.rico_hamdi.roomf110;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import android.util.Log;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import utt.rico_hamdi.roomf110.aws.AwsConnectionHandler;
import utt.rico_hamdi.roomf110.aws.AwsService;

import android.view.MenuItem;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;
import com.amazonaws.services.iot.AWSIotClient;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.services.iot.model.AttachPolicyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    static final String LOG_TAG = MainActivity.class.getCanonicalName();

    AWSIotMqttManager mqttManager;

    //private String clientID = UUID.randomUUID().toString();
    //AWSIotMqttManager mqttManager = new AWSIotMqttManager(clientID, "a2925nglmmp0xz-ats.iot.eu-west-1.amazonaws.com");


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    return true;
                case R.id.navigation_lights:
                    Intent intent2 = new Intent(MainActivity.this, LightsMenuActivity.class);
                    startActivity(intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                    break;
                case R.id.navigation_more:
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AwsConnectionHandler connection = new AwsConnectionHandler(this);
        mqttManager = connection.getMqttManager();


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_dashboard);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }
}

