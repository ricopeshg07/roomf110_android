package utt.rico_hamdi.roomf110;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import utt.rico_hamdi.roomf110.models.HueObject;
import utt.rico_hamdi.roomf110.models.LightsGroup;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LightEditFragment extends Fragment {

    public HueObject editing_object;

    private OnLightEditFragmentInteractionListener mListener;
    public static LightEditFragment newInstance() {
        LightEditFragment frag = new LightEditFragment();
        frag.editing_object = null;
        return frag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLightEditFragmentInteractionListener) {
            mListener = (OnLightEditFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLightEditFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.light__edit_fragment, container, false);
        /***TextView textView = mView.findViewById(R.id.hueobject_name);
        textView.setText(editing_object.name);
        if(editing_object instanceof LightsGroup)
            textView.setVisibility(View.GONE);
        else
            textView.setVisibility(View.VISIBLE);
         ***/
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }

    public interface OnLightEditFragmentInteractionListener {
        void modifyName();
        void modifyBrightness();
        void modifyColor();
    }
}
