package utt.rico_hamdi.roomf110;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LightsGroupFragment.onLightsGroupListener} interface
 * to handle interaction events.
 * Use the {@link LightsGroupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LightsGroupFragment extends Fragment {

    private onLightsGroupListener mListener;
    private ImageView addButton;

    public LightsGroupFragment() {
        // Required empty public constructor
    }

    public static LightsGroupFragment newInstance() {
        LightsGroupFragment fragment = new LightsGroupFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView =  inflater.inflate(R.layout.fragment_lights_group, container, false);
        addButton = mView.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.setAddFragmentGroup();
            }
        });
        return mView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onLightsGroupListener) {
            mListener = (onLightsGroupListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface onLightsGroupListener {
        void onLightsGroupInteraction();
        void setAddFragmentGroup();
    }
}
