package utt.rico_hamdi.roomf110;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import utt.rico_hamdi.roomf110.models.HueObject;
import utt.rico_hamdi.roomf110.models.LightsGroup;
import utt.rico_hamdi.roomf110.models.Scene;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */

public class HueObjectListFragment extends Fragment {

    public enum HUE_OBJECT_TYPE {
        LIGHT,
        LIGHTS_GROUP,
        SCENE,
    }

    private static final String ARG_HUE_OBJECT_TYPE = "type";
    private static final String ARG_HUE_OBJECT_CHECKABLE = "checkable";

    protected OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HueObjectListFragment() {
    }

    public static HueObjectListFragment newInstance() {
        HueObjectListFragment fragment = new HueObjectListFragment();
        return fragment;
    }

    public void subscribeListTopic() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /***awsSubscription****/
        subscribeListTopic();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hueobject_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
                recyclerView.setLayoutManager(new LinearLayoutManager(context));

            /***
             * MQTT AWS getHUEOBJECTYPE
             */
            switch(mListener.getHueObjectType()){
                case LIGHT:
                    break;
                case LIGHTS_GROUP:
                    break;
                case SCENE:
                    break;
            }

            ArrayList<HueObject> myLights = new ArrayList<>();
            myLights.add(new HueObject("1", "test1"));

            myLights.add(new Scene("2", "test2"));

            myLights.add(new LightsGroup("3", "test3"));
            recyclerView.setAdapter(new MyHueObjectRecyclerViewAdapter(myLights, mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLightEditFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(HueObject item);
        void setEditLightState(HueObject object);
        void addLightId(String lightId);
        void removeLightId(String lightId);
        void recallScene(String sceneId);
        int getRecyclerLayout();
        HUE_OBJECT_TYPE getHueObjectType();
    }
}
