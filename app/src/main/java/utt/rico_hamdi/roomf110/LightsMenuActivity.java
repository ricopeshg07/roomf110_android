package utt.rico_hamdi.roomf110;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import utt.rico_hamdi.roomf110.models.HueObject;
import utt.rico_hamdi.roomf110.models.LightsGroup;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LightsMenuActivity extends FragmentActivity implements LightFragment.OnFragmentInteractionListener,
        HueObjectListFragment.OnListFragmentInteractionListener,
        LightsGroupFragment.onLightsGroupListener,
        SceneFragment.OnSceneFragmentListener,
        LightEditFragment.OnLightEditFragmentInteractionListener,
        AddFragment.OnAddFragmentInteractionListener
{

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    Intent intent = new Intent(LightsMenuActivity.this, MainActivity.class);
                    startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                    return true;
                case R.id.navigation_lights:
                    return true;
                case R.id.navigation_more:
                    return true;
            }
            return false;
        }
    };

    /**
     * The {@link androidx.viewpager.widget.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * androidx.fragment.app.FragmentStatePagerAdapter.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lights_menu);
        newGroupLightIds = new LinkedList<>();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_lights);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mViewPager = (ViewPager) findViewById(R.id.lights_container);
        this.setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(LightFragment.newInstance());
        adapter.addFragment(LightsGroupFragment.newInstance());
        adapter.addFragment(SceneFragment.newInstance());
        adapter.addFragment(LightEditFragment.newInstance());
        adapter.addFragment(AddFragment.newInstance());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void setAllEditFragment() {
        setEditLightState(LightsGroup.getGroupAll());
    }

    @Override
    public void onListFragmentInteraction(HueObject item) {

    }

    @Override
    public void setEditLightState(HueObject object) {
        SectionsPagerAdapter adapter = (SectionsPagerAdapter) mViewPager.getAdapter();
        LightEditFragment frag = (LightEditFragment) adapter.getItem(3);
        frag.editing_object = object;
        mViewPager.setCurrentItem(3, false);
    }


    public LinkedList<String> newGroupLightIds;
    @Override
    public void addLightId(String lightId) {
        newGroupLightIds.add(lightId);
    }

    @Override
    public void removeLightId(String lightId) {
        newGroupLightIds.remove(lightId);
    }

    @Override
    public void recallScene(String sceneId) {

    }

    @Override
    public int getRecyclerLayout() {
        SectionsPagerAdapter adapter = (SectionsPagerAdapter) mViewPager.getAdapter();
        Fragment currentFragment = adapter.getItem(mViewPager.getCurrentItem());
        if(currentFragment instanceof AddFragment)
            return R.layout.fragment_hueobject_checkable;
        return R.layout.fragment_hueobject;
    }

    @Override
    public HueObjectListFragment.HUE_OBJECT_TYPE getHueObjectType() {
        SectionsPagerAdapter adapter = (SectionsPagerAdapter) mViewPager.getAdapter();
        Fragment currentFragment = adapter.getItem(mViewPager.getCurrentItem());
        if(currentFragment instanceof LightFragment)
            return HueObjectListFragment.HUE_OBJECT_TYPE.LIGHT;
        if(currentFragment instanceof AddFragment)
            return HueObjectListFragment.HUE_OBJECT_TYPE.LIGHT;
        if(currentFragment instanceof SceneFragment)
            return HueObjectListFragment.HUE_OBJECT_TYPE.SCENE;
        if(currentFragment instanceof LightsGroupFragment)
            return HueObjectListFragment.HUE_OBJECT_TYPE.LIGHTS_GROUP;
        return HueObjectListFragment.HUE_OBJECT_TYPE.LIGHT;
    }

    @Override
    public void onLightsGroupInteraction() {

    }

    private void setAddFragment(boolean hideList){

    }
    @Override
    public void setAddFragmentGroup() {
        mViewPager.setCurrentItem(4, false);
    }

    @Override
    public void setAddFragmentScene() {

    }

    @Override
    public void onAddFragmentInteraction() {

    }

    @Override
    public void saveGroup() {

    }

    @Override
    public void saveScene() {

    }

    @Override
    public void modifyName() {

    }

    @Override
    public void modifyBrightness() {

    }

    @Override
    public void modifyColor() {

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size() ;
        }

        public boolean canScrollHorizontally(int direction) {
            return false;
        }
    }
}
