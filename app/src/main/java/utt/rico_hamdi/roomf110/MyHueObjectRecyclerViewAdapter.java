package utt.rico_hamdi.roomf110;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import utt.rico_hamdi.roomf110.HueObjectListFragment.OnListFragmentInteractionListener;
import utt.rico_hamdi.roomf110.models.HueObject;
import utt.rico_hamdi.roomf110.models.Scene;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link HueObject} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class MyHueObjectRecyclerViewAdapter extends RecyclerView.Adapter<MyHueObjectRecyclerViewAdapter.ViewHolder> {

    private final List<HueObject> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyHueObjectRecyclerViewAdapter(List<HueObject> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId = mListener.getRecyclerLayout();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).name);
        if(mListener.getRecyclerLayout() == R.layout.fragment_hueobject_checkable)
        {
            holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                        mListener.addLightId(holder.mItem.id);
                    else
                        mListener.removeLightId(holder.mItem.id);
                }
            });
        }
        else
        {
            if(holder.mItem instanceof Scene)
            {
                holder.button.setImageResource(R.drawable.ic_delete_black_24dp);
                holder.switchView.setVisibility(View.GONE);
                holder.mIdView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.recallScene(holder.mItem.id);
                        }
                    }
                });
            }
            else
            {
                holder.button.setVisibility(View.VISIBLE);
                holder.switchView.setVisibility(View.VISIBLE);
                holder.switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        // do something, the isChecked will be
                        // true if the switch is in the On position
                    }
                });
                holder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.setEditLightState(holder.mItem);
                        }
                    }
                });
            }
        }

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final Switch switchView;
        public final ImageButton button;
        public final CheckBox checkbox;
        public HueObject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.hueobject_name);
            switchView = view.findViewById(R.id.hueobject_switch);
            button = view.findViewById(R.id.hueobject_edit_button);
            checkbox = view.findViewById(R.id.checkBox);
        }

        @Override
        public String toString() {
            return super.toString() + " name: "+ mItem.name;
        }
    }
}
